const R = require('ramda')
const data = require('./forecast2.json')

function getUTCSeconds(offset) {
  const today = new Date()
  const date = new Date()
  date.setDate(today.getDate() + offset)
  return date.setHours(-today.getTimezoneOffset() / 60, 0, 0, 0) / 1000
}

const tomorrowUTCSeconds = getUTCSeconds(+1)
const dayAfterTomorrowUTCSeconds = getUTCSeconds(+2)
const result = R.compose(
  (i) => ({
    tempMin: Math.round(R.reduce(R.max, R.head(i), i)),
    tempMax: Math.round(R.reduce(R.min, R.head(i), i)),
  }),
  R.map(R.path(['main', 'temp'])),
  R.filter((i) => i.dt >= tomorrowUTCSeconds && i.dt < dayAfterTomorrowUTCSeconds),
  R.path(['list'])
)(data)

console.log(result)

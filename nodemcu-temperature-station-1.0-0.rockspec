package = "nodemcu-temperature-station"
version = "1.0-0"
source = {
   url = "..." -- We don't have one yet
}
description = {
   summary = "Temperature station for NodeMCU",
   detailed = [[
     Temperature station for NodeMCU
   ]],
   homepage = "http://...", -- We don't have one yet
   license = "MIT" -- or whatever you like
}
dependencies = {
   "lua >= 5.1, < 5.4",
   "lua-cjson = 2.1.2-1",
   "luacheck = dev-1",
   -- If you depend on other rocks, add them here
}
build = {
   -- We'll start here.
}

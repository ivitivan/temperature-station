clean:
	rm -rf ./dist/

build:
	make clean && mkdir -p ./dist/ && nodemcu-luapack ./src/init.lua > ./dist/init.lua

upload:
	nodemcu-tool upload -o ./dist/init.lua

run:
	make build && make upload && nodemcu-tool reset && nodemcu-tool terminal

erase_flash:
	esptool --port /dev/ttyUSB0 erase_flash

flash:
	esptool --port /dev/ttyUSB0 write_flash -fm dio 0x0000 /home/vit/Projects/nodemcu-firmware/bin/nodemcu_integer_master_20180303-1903.bin

terminal:
	nodemcu-tool terminal

test:
	./lua_modules/bin/busted --helper=set_paths -p .test .

coverage:
	./lua_modules/bin/busted --helper=set_paths -c -p .test .

luacov:
	./lua_modules/bin/luacov

lint:
	luacheck ./src/**/*.lua

local config = require('../config/config')

local function parseCurrentWeather(data)
  local currentTemp = math.floor(data.main.temp)

  return tostring(currentTemp)
end

local function floatsToInt(data)
  return string.gsub(data, '%.%d+', '')
end

local function getCurrentTemp(callback)
  local url = config.CURRENT_WEATHER_URL .. '&appid=' .. config.OPEN_WEATHER_APP_ID

  http.get(url, nil, function(code, data)
    if (code < 0) then
      callback(code, data)
    else
      local integerData = floatsToInt(data)
      local temp = parseCurrentWeather(sjson.decode(integerData))
      callback(nil, temp)
    end
  end)
end

local function start(onTempAvailable)
  local timer = tmr.create()

  timer:register(config.FORECASTER_TIMEOUT, tmr.ALARM_AUTO, function ()
    getCurrentTemp(function(errCode, temp)
      if (errCode) then
        print(errCode)
      else
        print('Temp: ' .. tostring(temp))
        onTempAvailable(temp)
      end
    end)
  end)
  timer:start()
end

return {
  start = start,
  floatsToInt = floatsToInt,
}

local forecaster = require('./forecaster')
local json = require('cjson')

local currentTempResponseMock = '{"coord":{"lon":37.62,"lat":55.75},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],"base":"stations","main":{"temp":1,"pressure":999,"humidity":69,"temp_min":1,"temp_max":1},"visibility":10000,"wind":{"speed":6,"deg":290},"clouds":{"all":75},"dt":1521815400,"sys":{"type":1,"id":7325,"message":0.0035,"country":"RU","sunrise":1521775373,"sunset":1521820209},"id":524901,"name":"Moscow","cod":200}' -- luacheck: ignore

local timerCallback
local timerRegisterSpy = spy.new(function(self, timeout, mode, callback) -- luacheck: ignore
  timerCallback = callback
end)
local timerStartSpy = spy.new(function() end)
local startCallback = spy.new(function() end)
local httpGetCallback

_G.tmr = { -- luacheck: ignore
  create = function()
    return {
      register = timerRegisterSpy,
      start = timerStartSpy,
    }
  end,
  ALARM_AUTO = 1,
}

_G.http = { -- luacheck: ignore
  get = function(url, headers, callback) -- luacheck: ignore
    httpGetCallback = callback
  end,
}

_G.wifi = { -- luacheck: ignore
  setmode = function() end,
  sta = {
    config = function() end,
  },
  STATION = 'station',
  eventmon = {
    register = function(self, timeout, mode, callback) end, -- luacheck: ignore
    STA_CONNECTED = 'sta_connected',
  },
}

_G.sjson = json -- luacheck: ignore

_G.print = spy.new(function() end) -- luacheck: ignore

describe('forecaster', function()
  describe('start', function()
    it('creates timer', function()
      spy.on(tmr, 'create')

      forecaster.start(startCallback)

      assert.spy(tmr.create).was.called()
    end)

    it('starts timer', function()
      forecaster.start(startCallback)

      assert.spy(timerStartSpy).was.called()
    end)

    it('requests temps', function()
      forecaster.start(startCallback)
      spy.on(http, 'get')
      timerCallback()

      assert.spy(http.get).was.called()
    end)

    it('runs callback on http data', function()
      forecaster.start(startCallback)
      timerCallback()
      httpGetCallback(1, currentTempResponseMock)

      assert.spy(startCallback).was_called_with('1')
    end)

    it('calls callback with error code if request error', function()
      forecaster.start(startCallback)
      timerCallback()
      httpGetCallback(-1, nil)

      assert.spy(print).was_called_with(-1)
    end)
  end)
end)

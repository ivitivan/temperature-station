local TemperatureStation = {}

function TemperatureStation:new(dependencies)
  local station = dependencies
  setmetatable(station, self)
  self.__index = self
  return station
end

function TemperatureStation.handleConnection(self)
  self.display:setup()

  self.forecaster.start(function(temp)
    self.display:show(temp)
  end)
end

function TemperatureStation.start(self)
  self.wifiStation.start({
    ssid = self.config.WIFI_SSID,
    pwd = self.config.WIFI_PASSWORD,
  })

  self.wifiStation.onConnection(function()
    self:handleConnection()
  end)
end

return TemperatureStation

local TemperatureStation = require('./main')
local Display = require('../display/display')
local config = require('../config/config')
local match = require('luassert.match')

describe('TemperatureStation', function()
  describe('constructor', function()
    it('should create an object with dependencies', function()
      local dependencies = {
        wifiStation = {},
        display = {},
        config = {},
        forecaster = {},
      }
      local station = TemperatureStation:new(dependencies)

      assert.are.same(station, dependencies)
    end)
  end)

  describe('handleConnection', function()
    it('should setup display', function()
      _G.tmr = {
        create = function()
          return {
            alarm = function() end,
          }
        end,
        ALARM_SINGLE = 1,
      }
      local display = {
        setup = function() end,
      }
      local dependencies = {
        wifiStation = {},
        display = {
          setup = function() end,
        },
        config = {
          DISPLAY_SDA = 1,
          DISPLAY_SCL = 2,
          DISPLAY_ADDR = 0x70,
        },
        forecaster = {
          start = function() end,
        },
      }
      spy.on(dependencies.display, 'setup')
      local station = TemperatureStation:new(dependencies)

      station:handleConnection()

      assert.spy(dependencies.display.setup).was_called_with(dependencies.display)
    end)

    it('should start the forecaster', function()
      _G.tmr = {
        create = function()
          return {
            alarm = function() end,
          }
        end,
        ALARM_SINGLE = 1,
      }
      local forecasterStartCallback
      local dependencies = {
        wifiStation = {},
        display = {
          setup = function() end,
          show = function() end,
        },
        config = {
          DISPLAY_SDA = 1,
          DISPLAY_SCL = 2,
          DISPLAY_ADDR = 0x70,
        },
        forecaster = {
          start = function(callback)
            forecasterStartCallback = callback
          end,
        },
      }
      spy.on(dependencies.forecaster, 'start')
      spy.on(dependencies.display, 'show')
      local station = TemperatureStation:new(dependencies)

      station:handleConnection()

      assert.spy(dependencies.forecaster.start).was.called()

      local temp = 16
      forecasterStartCallback(temp)

      assert.spy(dependencies.display.show).was_called_with(dependencies.display, temp)
    end)
  end)

  describe('start', function()
    it('should start wifi', function()
      _G.tmr = {
        create = function()
          return {
            alarm = function() end,
          }
        end,
        ALARM_SINGLE = 1,
      }
      local dependencies = {
        wifiStation = {
          start = function() end,
          onConnection = function() end,
        },
        display = {
          setup = function() end,
          show = function() end,
        },
        config = config,
        forecaster = {
          start = function(callback) end,
        },
      }
      spy.on(dependencies.wifiStation, 'start')

      local station = TemperatureStation:new(dependencies)

      station:start()

      assert.spy(dependencies.wifiStation.start).was_called_with({
        ssid = config.WIFI_SSID,
        pwd = config.WIFI_PASSWORD,
      })
    end)
    it('should call handleConnection on wifi connection', function()
      _G.tmr = {
        create = function()
          return {
            alarm = function() end,
          }
        end,
        ALARM_SINGLE = 1,
      }
      local wifiStationOnConnectionCallback
      local dependencies = {
        wifiStation = {
          start = function() end,
          onConnection = function(callback)
            wifiStationOnConnectionCallback = callback
          end,
        },
        display = {
          setup = function() end,
          show = function() end,
        },
        config = config,
        forecaster = {
          start = function(callback) end,
        },
      }
      spy.on(dependencies.wifiStation, 'onConnection')

      local station = TemperatureStation:new(dependencies)

      spy.on(station, 'handleConnection')

      station:start()

      assert.spy(dependencies.wifiStation.onConnection).was.called()

      wifiStationOnConnectionCallback()

      assert.spy(station.handleConnection).was.called()
    end)
  end)
end)

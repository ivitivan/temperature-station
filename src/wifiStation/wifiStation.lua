local function start(config)
  wifi.setmode(wifi.STATION)
  wifi.sta.config(config)
end

local function onConnection(callback)
  wifi.eventmon.register(wifi.eventmon.STA_CONNECTED, callback)
end

return {
  start = start,
  onConnection = onConnection,
}

_G.wifi = { -- luacheck: ignore
  setmode = function() end,
  sta = {
    config = function() end,
  },
  STATION = 'station',
  eventmon = {
    register = function() end,
    STA_CONNECTED = 'sta_connected',
  },
}

local wifiStation = require('./wifiStation')

describe('wifiStation', function()
  describe('start', function()
    it('start calls setmode', function()
      spy.on(wifi, 'setmode')

      wifiStation.start({
        ssid = 'asdf',
        pwd = 'asdf',
      })

      assert.spy(wifi.setmode).was_called_with('station')
    end)

    it('start calls sta.config', function()
      local config = {
        ssid = 'wifi',
        pwd = 'password!',
      }
      spy.on(wifi.sta, 'config')

      wifiStation.start(config)

      assert.spy(wifi.sta.config).was_called_with(config)
    end)
  end)

  describe('onConnection', function()
    it('registers callback', function()
      local callback = function() end

      spy.on(wifi.eventmon, 'register')

      wifiStation.onConnection(callback)

      assert.spy(wifi.eventmon.register).was_called_with(wifi.eventmon.STA_CONNECTED, callback)
    end)
  end)
end)

local function parseToDisplayFormat(str)
  local arr = {}
  local strIndex = str:len()

  local i = 8
  while (i > 0) do
    local c = strIndex > 0 and str:sub(strIndex, strIndex) or ''

    if (c == '.') then arr[i] = c
    elseif (i % 2 == 0) then
      arr[i] = ''
      arr[i - 1] = c
      i = i - 1
    else
      arr[i] = c
    end

    strIndex = strIndex - 1
    i = i - 1
  end

  return arr
end

local function fromHumanToMachineNumber(s)
  local symbolMap = {
    ['0'] = 0x3f,
    ['0.'] = 0xbf,
    ['1'] = 0x06,
    ['1.'] = 0x86,
    ['2'] = 0x5b,
    ['2.'] = 0xdb,
    ['3'] = 0x4f,
    ['3.'] = 0xcf,
    ['4'] = 0x66,
    ['4.'] = 0xe6,
    ['5'] = 0x6d,
    ['5.'] = 0xed,
    ['6'] = 0x7d,
    ['6.'] = 0xfd,
    ['7'] = 0x07,
    ['7.'] = 0x87,
    ['8'] = 0x7f,
    ['8.'] = 0xff,
    ['9'] = 0x6f,
    ['9.'] = 0xef,
    ['-'] = 0x40,
    [''] = 0x00,
  }

  return symbolMap[s]
end

return {
  parseToDisplayFormat = parseToDisplayFormat,
  fromHumanToMachineNumber = fromHumanToMachineNumber,
}

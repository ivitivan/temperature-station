local displayFormatConverter = require('./displayFormatConverter')

describe('displayFormatConverter', function()
  describe('parseToDisplayFormat', function()
    it('should convert "1234" to {"1", "", "2", "", "3", "", "4", ""}', function()
      local actual = displayFormatConverter.parseToDisplayFormat('1234')
      local expected = {'1', '', '2', '', '3', '', '4', ''}

      assert.are.same(actual, expected)
    end)

    it('should convert "1.2.3.4." to {"1", ".", "2", ".", "3", ".", "4", "."}', function()
      local actual = displayFormatConverter.parseToDisplayFormat('1.2.3.4.')
      local expected = {'1', '.', '2', '.', '3', '.', '4', '.'}

      assert.are.same(actual, expected)
    end)
  end)

  describe('fromHumanToMachineNumber', function()
    it('should convert "0" to 0x3f', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('0')
      local expected = 0x3f

      assert.are.same(actual, expected)
    end)

    it('should convert "0." to 0xbf', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('0.')
      local expected = 0xbf

      assert.are.same(actual, expected)
    end)

    it('should convert "1" to 0x06', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('1')
      local expected = 0x06

      assert.are.same(actual, expected)
    end)

    it('should convert "1." to 0x86', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('1.')
      local expected = 0x86

      assert.are.same(actual, expected)
    end)

    it('should convert "2" to 0x5b', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('2')
      local expected = 0x5b

      assert.are.same(actual, expected)
    end)

    it('should convert "2." to 0xdb', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('2.')
      local expected = 0xdb

      assert.are.same(actual, expected)
    end)

    it('should convert "3" to 0x4f', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('3')
      local expected = 0x4f

      assert.are.same(actual, expected)
    end)

    it('should convert "3." to 0xcf', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('3.')
      local expected = 0xcf

      assert.are.same(actual, expected)
    end)

    it('should convert "4" to 0x66', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('4')
      local expected = 0x66

      assert.are.same(actual, expected)
    end)

    it('should convert "4." to 0xe6', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('4.')
      local expected = 0xe6

      assert.are.same(actual, expected)
    end)

    it('should convert "5" to 0x6d', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('5')
      local expected = 0x6d

      assert.are.same(actual, expected)
    end)

    it('should convert "5." to 0xed', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('5.')
      local expected = 0xed

      assert.are.same(actual, expected)
    end)

    it('should convert "6" to 0x7d', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('6')
      local expected = 0x7d

      assert.are.same(actual, expected)
    end)

    it('should convert "6." to 0xfd', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('6.')
      local expected = 0xfd

      assert.are.same(actual, expected)
    end)

    it('should convert "7" to 0x07', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('7')
      local expected = 0x07

      assert.are.same(actual, expected)
    end)

    it('should convert "7." to 0x87', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('7.')
      local expected = 0x87

      assert.are.same(actual, expected)
    end)

    it('should convert "8" to 0x7f', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('8')
      local expected = 0x7f

      assert.are.same(actual, expected)
    end)

    it('should convert "8." to 0xff', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('8.')
      local expected = 0xff

      assert.are.same(actual, expected)
    end)

    it('should convert "9" to 0x6f', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('9')
      local expected = 0x6f

      assert.are.same(actual, expected)
    end)

    it('should convert "9." to 0xef', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('9.')
      local expected = 0xef

      assert.are.same(actual, expected)
    end)

    it('should convert "-" to 0x40', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('-')
      local expected = 0x40

      assert.are.same(actual, expected)
    end)

    it('should convert "" to 0x00', function()
      local actual = displayFormatConverter.fromHumanToMachineNumber('')
      local expected = 0x00

      assert.are.same(actual, expected)
    end)
  end)
end)

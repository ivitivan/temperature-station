local promise = require('../../lua_modules/promise/promise')

local function sync()
  local p = promise.new()

  sntp.sync(
    nil,
    function(sec)
      p:resolve(sec)
    end,
    function(errorCode, errorMessage) -- luacheck: ignore
      p:reject(errorMessage)
    end
  )

  return p
end

return {
  sync = sync,
}

local displayFormatConverter = require('../displayFormatConverter/displayFormatConverter')

local Display = {}

function Display:new(params)
  local display = {
    id = 0,
    sda = params.sda,
    scl = params.scl,
    addr = params.addr,
  }
  setmetatable(display, self)
  self.__index = self
  return display
end

function Display.message(self, addr, mode, messageList)
  i2c.start(self.id)
  i2c.address(self.id, addr, mode)

  for i,v in ipairs(messageList) do
    i2c.write(self.id, v)
  end

  i2c.stop(self.id)
end

-- TODO: use brightness level
function Display.setBrightness(self, brightness)
  if (brightness > 15) then return end

  local brightnessCommand = 0xef
  self:message(self.addr, i2c.TRANSMITTER, {brightnessCommand})
end

-- TODO: use b param
function Display.blink(self, b)
  if (b > 3) then return end

  local dontBlinkCommand = 0x81
  self:message(self.addr, i2c.TRANSMITTER, {dontBlinkCommand})
end

function Display.showDigit(self, digit, value)
  self:message(self.addr, i2c.TRANSMITTER, {digit, value})
end

function Display.startOscillation(self)
  local startOscillationCommand = 0x21
  self:message(self.addr, i2c.TRANSMITTER, {startOscillationCommand})
end

function Display.setup(self)
  i2c.setup(self.id, self.sda, self.scl, i2c.SLOW)

  self:startOscillation()
  self:setBrightness(15)
  self:blink(0)
end

function Display.show(self, str)
  local arr = displayFormatConverter.parseToDisplayFormat(str)

  for i = 0, 7, 2 do
    local s = arr[i + 1] .. arr[i + 2]
    local symbol = displayFormatConverter.fromHumanToMachineNumber(s)

    self:showDigit(6 - i, symbol)
  end
end

return Display

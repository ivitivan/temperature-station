local Display = require('./display')

local addr = 0x70
local id = 0
local sda = 1
local scl = 2

local function createDefaultDisplay()
  return Display:new({
    sda = sda,
    scl = scl,
    addr = addr,
  })
end

local function getDefaultI2cMock()
  return {
    start = function() end,
    setup = function() end,
    address = function() end,
    write = function() end,
    stop = function() end,
    TRANSMITTER = 1,
    SLOW = 2,
  }
end

describe('Display', function()
  describe('constructor', function()
    it('should create a display object', function()
      local display = createDefaultDisplay()

      assert.are.same(display.sda, sda)
      assert.are.same(display.scl, scl)
      assert.are.same(display.addr, addr)
    end)
  end)

  describe('message', function()
    it('should send an i2c message', function()
      local display = createDefaultDisplay()
      local callSequence = {}
      local callSequenceIndex = 1

      _G.i2c = { -- luacheck: ignore
        start = function()
          table.insert(callSequence, 'start')
          callSequenceIndex = callSequenceIndex + 1
        end,
        setup = function() end,
        address = function()
          table.insert(callSequence, 'address')
          callSequenceIndex = callSequenceIndex + 1
        end,
        write = function()
          table.insert(callSequence, 'write')
          callSequenceIndex = callSequenceIndex + 1
        end,
        stop = function()
          table.insert(callSequence, 'stop')
          callSequenceIndex = callSequenceIndex + 1
        end,
        TRANSMITTER = 1,
        SLOW = 2,
      }

      spy.on(i2c, 'start')
      spy.on(i2c, 'address')
      spy.on(i2c, 'write')
      spy.on(i2c, 'stop')

      display:message(addr, i2c.TRANSMITTER, {'Hello', 'there', '!'})

      assert.spy(i2c.start).was_called_with(id)
      assert.spy(i2c.address).was_called_with(id, addr, i2c.TRANSMITTER)
      assert.spy(i2c.write).was.called_with(id, 'Hello')
      assert.spy(i2c.write).was.called_with(id, 'there')
      assert.spy(i2c.write).was.called_with(id, '!')
      assert.spy(i2c.stop).was_called_with(id)
      assert.are.same(callSequence, {'start', 'address', 'write', 'write', 'write', 'stop'})
    end)
  end)

  describe('setBrightness', function()
    it('should send message to change brightness level', function()
      _G.i2c = getDefaultI2cMock() -- luacheck: ignore
      local display = createDefaultDisplay()

      spy.on(display, 'message')

      display:setBrightness(15)

      assert.spy(display.message).was_called_with(display, addr, i2c.TRANSMITTER, {0xef})
    end)
  end)

  describe('blink', function()
    it('should send message to change blinking method', function()
      _G.i2c = getDefaultI2cMock() -- luacheck: ignore
      local display = createDefaultDisplay()

      spy.on(display, 'message')

      display:blink(0)

      assert.spy(display.message).was_called_with(display, addr, i2c.TRANSMITTER, {0x81})
    end)
  end)

  describe('showDigit', function()
    it('should send message to show a digit', function()
      _G.i2c = getDefaultI2cMock() -- luacheck: ignore
      local display = createDefaultDisplay()

      spy.on(display, 'message')

      display:showDigit(1, 0x3f)

      assert.spy(display.message).was_called_with(display, addr, i2c.TRANSMITTER, {1, 0x3f})
    end)
  end)

  describe('startOscillation', function()
    it('should send message to turn on oscillation', function()
      _G.i2c = getDefaultI2cMock() -- luacheck: ignore
      local display = createDefaultDisplay()

      spy.on(display, 'message')

      display:startOscillation()

      assert.spy(display.message).was_called_with(display, addr, i2c.TRANSMITTER, {0x21})
    end)
  end)

  describe('setup', function()
    it('should setup i2c, start oscillation, set brightness and blinking mode', function()
      _G.i2c = getDefaultI2cMock() -- luacheck: ignore
      local display = createDefaultDisplay()

      spy.on(i2c, 'setup')
      spy.on(display, 'startOscillation')
      spy.on(display, 'setBrightness')
      spy.on(display, 'blink')

      display:setup()

      assert.spy(i2c.setup).was_called_with(id, sda, scl, i2c.SLOW)
      assert.spy(display.startOscillation).was.called()
      assert.spy(display.setBrightness).was_called_with(display, 15)
      assert.spy(display.blink).was_called_with(display, 0)
    end)
  end)

  describe('show', function()
    it('should show numbers for a 4 digit display', function()
      _G.i2c = getDefaultI2cMock() -- luacheck: ignore
      local display = createDefaultDisplay()

      spy.on(display, 'showDigit')

      display:show('1234')

      assert.spy(display.showDigit).was.called_with(display, 6, 0x06)
      assert.spy(display.showDigit).was.called_with(display, 4, 0x5b)
      assert.spy(display.showDigit).was.called_with(display, 2, 0x4f)
      assert.spy(display.showDigit).was.called_with(display, 0, 0x66)
    end)
  end)
end)

--local display = require('./display')
--local match = require('luassert.match')

--local addr = 0x70
--local id = 0

--local sda = 1
--local scl = 2

--local function getDefaultI2cMock()
  --return {
    --start = function() end,
    --setup = function() end,
    --address = function() end,
    --write = function() end,
    --stop = function() end,
    --TRANSMITTER = 1,
    --SLOW = 2,
  --}
--end

--describe('display', function()
  --describe('message', function()
    --it('should make i2c calls in appropriate order', function()
      --local callSequence = {}
      --local callSequenceIndex = 1

      --_G.i2c = { -- luacheck: ignore
        --start = function()
          --table.insert(callSequence, 'start')
          --callSequenceIndex = callSequenceIndex + 1
        --end,
        --setup = function() end,
        --address = function()
          --table.insert(callSequence, 'address')
          --callSequenceIndex = callSequenceIndex + 1
        --end,
        --write = function()
          --table.insert(callSequence, 'write')
          --callSequenceIndex = callSequenceIndex + 1
        --end,
        --stop = function()
          --table.insert(callSequence, 'stop')
          --callSequenceIndex = callSequenceIndex + 1
        --end,
        --TRANSMITTER = 1,
        --SLOW = 2,
      --}

      --spy.on(i2c, 'start')
      --spy.on(i2c, 'address')
      --spy.on(i2c, 'write')
      --spy.on(i2c, 'stop')

      --display.message(addr, i2c.TRANSMITTER, {'Hello', 'there', '!'})

      --assert.spy(i2c.start).was_called_with(id)
      --assert.spy(i2c.address).was_called_with(id, addr, i2c.TRANSMITTER)
      --assert.spy(i2c.write).was.called_with(id, 'Hello')
      --assert.spy(i2c.write).was.called_with(id, 'there')
      --assert.spy(i2c.write).was.called_with(id, '!')
      --assert.spy(i2c.stop).was_called_with(id)
      --assert.are.same(callSequence, {'start', 'address', 'write', 'write', 'write', 'stop'})
    --end)
  --end)

  --describe('setup', function()
    --it('should init i2c', function()
      --_G.i2c = getDefaultI2cMock()
      --spy.on(i2c, 'setup')

      --display.setup(sda, scl, addr)

      --assert.spy(i2c.setup).was_called_with(id, sda, scl, i2c.SLOW)
    --end)

    --it('should start oscillator', function()
      --_G.i2c = getDefaultI2cMock()
      --spy.on(display, 'message')

      --display.setup(sda, scl, addr)

      --assert.spy(display.message).was.called()
      ----assert.spy(display.message).was.called_with(addr, i2c.TRANSMITTER, match.is_same({0x21}))
    --end)

    ----it('should set brightness', function()
      ----local display = require('./display')
      ----_G.i2c = getDefaultI2cMock()
      ----spy.on(display, 'setBrightness')

      ----display.setup(sda, scl, addr)

      ----assert.spy(display.setBrightness).was.called()
      ------assert.spy(display.message).was.called_with(addr, i2c.TRANSMITTER, match.is_same({0x21}))
    ----end)
  --end)
--end)

local wifiStation = require('./wifiStation/wifiStation')
local Display = require('./display/display')
local config = require('./config/config')
local forecaster = require('./forecaster/forecaster')
local TemperatureStation = require('./main/main')

local display = Display:new({
  sda = config.DISPLAY_SDA,
  scl = config.DISPLAY_SCL,
  addr = config.DISPLAY_ADDR,
})

local station = TemperatureStation:new({
  wifiStation = wifiStation,
  display = display,
  config = config,
  forecaster = forecaster,
})

station:start()

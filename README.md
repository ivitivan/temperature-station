# NodeMCU temperature station

Written in Lua, using [http://openweathermap.org/](http://openweathermap.org/) API.

## Required dependencies

- nodemcu-luapack
- nodemcu-tools

## Run

Upload code to NodeMCU and run it:

```bash
make run
```

## License

MIT

## Links

- http://www.partsnotincluded.com/electronics/controlling-led-matrix-with-the-ht16k33/
- https://cdn-shop.adafruit.com/datasheets/ht16K33v110.pdf
